# Bellicious

Trouver un restaurant proche de chez vous avec Bell

## Pour débuter

Cloner le repo:
```
git clone https://m3can0@gitlab.com/m3can0/bellicious.git
```

### Prerequis

N/AP

```
N/AP
```

### Installation

N/AP

```
N/AP
```

## Tests Unitaire

N/AP

### Test 1

Explication test

```
Exemple
```

## Deploiement

* [AWS -- Static Webpage](http://docs.aws.amazon.com/AmazonS3/latest/dev/website-hosting-custom-domain-walkthrough.html#root-domain-walkthrough-s3-tasks) -- Doc pour creation de site web static
* [Gitlab CI -- Tests](https://about.gitlab.com/2016/07/29/the-basics-of-gitlab-ci/#run-our-first-test-inside-ci) -- Doc GitLab pour tests dans CI
* [Gitlab CI -- Déploiement](https://about.gitlab.com/2016/08/26/ci-deployment-and-environments/) -- Doc GitLab pour déploiement dans CI
* [AWS -- Create IAM user](http://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html) -- Creation d'un utilisateur pour le CI

* [Granting Access to a Single S3 Bucket Using Amazon IAM](http://mikeferrier.com/2011/10/27/granting-access-to-a-single-s3-bucket-using-amazon-iam/) -- Mike Ferrier tutorial

## Built With

* [Animated Background Plugin](http://matthew.wagerfield.com/flat-surface-shader/) - Matthew Wagerfield

## Contributions

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Version

TBD -- automatiser version avec CI 

## Auteur

* **Nataniel P-Montpetit** - *Initial work* - [m3can0](nataniel@napworkshop.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Inspirations

* Inspiration


package com.napworkshop.bellicious;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.napworkshop.bellicious.models.SearchResponse;
import com.napworkshop.bellicious.networks.VolleyNetwork;

import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * VolleyNetworkTest
 * Classe qui test les call network
 *
 * @author Nataniel P-Monteptit
 * @created 2018-07-24
 */
@RunWith(AndroidJUnit4.class)
public class VolleyNetworkTest {
    public final static String TAG = VolleyNetworkTest.class.getSimpleName();

    @Test
    public void testSearchQuery() {
        Context context = InstrumentationRegistry.getTargetContext();
        //Create and do the request
        YelpSearchRequest request = new YelpSearchRequest("term=delis", new Response.Listener<SearchResponse>() {
            @Override
            public void onResponse(SearchResponse response) {
                Log.i(TAG, String.format("Yelp SearchResponse: %s", response.toString()));
            }
        }, new Response.ErrorListener(){
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Yelp Error(${response.networkResponse.statusCode}): ${response.networkResponse.data}");
            }
        } );

        //Fail if no internet
        VolleyNetwork network = new VolleyNetwork(context);
        assertTrue("No internet available", !network.addRequest(request));
    }
}

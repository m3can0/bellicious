package com.napworkshop.bellicious.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.util.Log
import com.napworkshop.bellicious.BusinessFragment
import com.napworkshop.bellicious.models.Business

/**
 * BusinessPagerAdapter
 * Class handling the adapter for the view pager and the fragment it contain
 *
 * @author m3can0
 * @created 2018-07-25
 */
class BusinessPagerAdapter(fm: FragmentManager, private val businessList: List<Business>) : FragmentStatePagerAdapter(fm) {
    val mTag = BusinessPagerAdapter::class.java.simpleName

    override fun getItem(newPosition: Int): Fragment {
        //Create a new fragment with the visible item
        val business= businessList[newPosition]
        Log.d(mTag,"getItem[$newPosition] => $business")
        return BusinessFragment.Builder.newInstance(business)
    }

    override fun getCount(): Int {
        return businessList.size
    }
}
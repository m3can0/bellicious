package com.napworkshop.bellicious.models

import com.google.gson.annotations.SerializedName

/**
 * SearchResponse
 * Object containing the response of a search query
 *
 * @author m3can0
 * @created 2018-07-24
 */
class SearchResponse(
        @SerializedName("total") val total: Int,
        @SerializedName("businesses") val businessList: List<Business>
){
    override fun toString(): String {
        return "SearchResponse(" +
                "total=$total, " +
                "businessList=$businessList)"
    }
}
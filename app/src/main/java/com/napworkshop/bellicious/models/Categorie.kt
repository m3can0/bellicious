package com.napworkshop.bellicious.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Categorie
 * Model for categories
 *
 * @author m3can0
 * @created 2018-07-26
 */
class Categorie(
    @SerializedName("title") val title: String
):Parcelable{
    constructor(parcel: Parcel): this(parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "Categorie(title='$title')"
    }

    companion object CREATOR : Parcelable.Creator<Categorie> {
        override fun createFromParcel(parcel: Parcel): Categorie {
            return Categorie(parcel)
        }

        override fun newArray(size: Int): Array<Categorie?> {
            return arrayOfNulls(size)
        }
    }
}
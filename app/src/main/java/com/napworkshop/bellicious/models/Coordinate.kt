package com.napworkshop.bellicious.models

import com.google.gson.annotations.SerializedName
import com.napworkshop.bellicious.interfaces.IUrlFormatter

/**
 * Coordinate
 * Class Coordinate of a place
 * By default Montreal location
 *
 * @author m3can0
 * @created 2018-07-24
 */
class Coordinate(
        @SerializedName("longitude") val longitude: Double = -73.5673,
        @SerializedName("latitude") val latitude: Double = 45.5017): IUrlFormatter {

    override fun formatToUrlParam(): String {
       return "?latitude=$latitude&longitude=$longitude"
    }
}
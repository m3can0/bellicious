package com.napworkshop.bellicious.models

import com.google.gson.annotations.SerializedName

/**
 * SearchResponse
 * Object containing the response of a search query
 *
 * @author m3can0
 * @created 2018-07-24
 */
class ReviewResponse(@SerializedName("reviews") val reviewList: List<Review> ){
    override fun toString(): String {
        return "ReviewResponse(" +
                "reviewList=$reviewList)"
    }
}
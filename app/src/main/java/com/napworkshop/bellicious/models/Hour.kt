package com.napworkshop.bellicious.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Hour
 * Classe qui fait quelque chose
 *
 * @author m3can0
 * @created 2018-07-26
 */
class Hour(@SerializedName("hours_type") val type: String, @SerializedName("open") val businessHourList: List<BusinessHour>): Parcelable {

    override fun toString(): String {
        return "Hour(type='$type', hourList='$businessHourList')"
    }

    constructor(parcel: Parcel): this( parcel.readString(), ArrayList()  ){
        parcel.writeTypedList(businessHourList)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(type)
        parcel.writeTypedList(businessHourList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Hour> {
        override fun createFromParcel(parcel: Parcel): Hour {
            return Hour(parcel)
        }

        override fun newArray(size: Int): Array<Hour?> {
            return arrayOfNulls(size)
        }
    }

    class BusinessHour(
            @SerializedName("day") val day: Int,
            @SerializedName("start") val start: String,
            @SerializedName("end") val end: String
    ):Parcelable{

        override fun toString(): String {
            return "(day=$day, start='$start', end='$end')"
        }

        constructor(parcel: Parcel): this( parcel.readInt(), parcel.readString(), parcel.readString()  )

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeInt(day)
            parcel.writeString(start)
            parcel.writeString(end)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<BusinessHour> {
            override fun createFromParcel(parcel: Parcel): BusinessHour {
                return BusinessHour(parcel)
            }

            override fun newArray(size: Int): Array<BusinessHour?> {
                return arrayOfNulls(size)
            }
        }
    }

}
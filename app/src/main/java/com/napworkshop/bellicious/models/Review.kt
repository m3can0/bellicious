package com.napworkshop.bellicious.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Review
 * Model for categories
 *
 * @author m3can0
 * @created 2018-07-26
 */
class Review(
    @SerializedName("text") val text: String
): Parcelable {
    constructor(parcel: Parcel): this(parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(text)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "Review(title='$text')"
    }

    companion object CREATOR : Parcelable.Creator<Review> {
        override fun createFromParcel(parcel: Parcel): Review {
            return Review(parcel)
        }

        override fun newArray(size: Int): Array<Review?> {
            return arrayOfNulls(size)
        }
    }
}
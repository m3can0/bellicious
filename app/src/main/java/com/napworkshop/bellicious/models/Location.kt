package com.napworkshop.bellicious.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Location
 * Model for the loaction of the business
 *
 * @author m3can0
 * @created 2018-07-25
 */
class Location(
        @SerializedName("city") val city: String,
        @SerializedName("display_address") val displayAddress: List<String>,
        @SerializedName("country") val country: String,
        @SerializedName("state") val state: String,
        @SerializedName("zip_code") val zipCode: String
):Parcelable{

    constructor(parcel: Parcel): this(
            parcel.readString(),
            ArrayList<String>(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()
    ){
        parcel.readStringList(displayAddress)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(city)
        parcel.writeStringList(displayAddress)
        parcel.writeString(country)
        parcel.writeString(state)
        parcel.writeString(zipCode)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "Location(city='$city', displayAddress=$displayAddress, country='$country', state='$state', zipCode='$zipCode')"
    }

    companion object CREATOR : Parcelable.Creator<Location> {
        override fun createFromParcel(parcel: Parcel): Location {
            return Location(parcel)
        }

        override fun newArray(size: Int): Array<Location?> {
            return arrayOfNulls(size)
        }
    }

}
package com.napworkshop.bellicious.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/**
 * Business
 * Classe qui fait quelque chose
 *
 * @author m3can0
 * @created 2018-07-24
 */
class Business(
        @SerializedName("id") val id: String,
        @SerializedName("name") val name: String,
        @SerializedName("image_url") val imageUrl: String?,
        @SerializedName("rating") val rating: Float,
        @SerializedName("display_phone") val phone: String,
        @SerializedName("location") val location: Location,
        @SerializedName("categories") val categoryList: List<Categorie>?,
        @SerializedName("hours") val hourList: List<Hour>?

): Parcelable{

    override fun toString(): String {
        return "Business(rating=$rating, name='$name', phone='$phone', categoryList='$categoryList', hourList='$hourList')"
    }

    constructor(parcel: Parcel): this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readFloat(),
        parcel.readString(),
        parcel.readParcelable(Location::class.java.classLoader),
        ArrayList<Categorie>(),
        ArrayList<Hour>()
    ){
        parcel.readTypedList(this.categoryList, Categorie)
        parcel.readTypedList(this.hourList, Hour)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(imageUrl)
        parcel.writeFloat(rating)
        parcel.writeString(phone)
        parcel.writeParcelable(location, flags)
        parcel.writeTypedList(categoryList)
        parcel.writeTypedList(hourList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Business> {
        override fun createFromParcel(parcel: Parcel): Business {
            return Business(parcel)
        }

        override fun newArray(size: Int): Array<Business?> {
            return arrayOfNulls(size)
        }
    }
}
//OK -- Ratings
//Hours of operation
//Snippet
//Deals

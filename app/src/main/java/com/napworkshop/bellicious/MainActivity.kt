package com.napworkshop.bellicious

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import com.napworkshop.bellicious.enums.SearchParameter
import com.napworkshop.bellicious.presenters.ISearchPresenter
import com.napworkshop.bellicious.models.Business
import com.napworkshop.bellicious.networks.VolleyNetwork
import android.app.Activity
import android.support.v4.view.ViewPager
import android.view.inputmethod.InputMethodManager
import com.napworkshop.bellicious.adapters.BusinessPagerAdapter
import com.napworkshop.bellicious.enums.SortTypeParameter
import com.napworkshop.bellicious.models.Coordinate


class MainActivity : AppCompatActivity(), ISearchPresenter.ISearchView {
    private val mTag = MainActivity::class.java.simpleName

    private val searchPresenter: ISearchPresenter by lazy{
        ISearchPresenter(VolleyNetwork.getInstance(this.applicationContext), this)
    }

    private val businessVp: ViewPager by lazy{ findViewById<ViewPager>(R.id.vp_business_list) }
    private val searchEt: EditText by lazy{ findViewById<EditText>(R.id.et_search_bar) }
    private val searchTypeSpr: Spinner by lazy{ findViewById<Spinner>(R.id.spr_search_type) }
    private val ratingRdb: RadioButton by lazy{ findViewById<RadioButton>(R.id.rdb_rating) }
    private val distanceRdb: RadioButton by lazy{ findViewById<RadioButton>(R.id.rdb_distance) }
    private val startSearchIb: ImageButton by lazy{ findViewById<ImageButton>(R.id.ib_search) }

    private val currentCoordinate = Coordinate()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Implements all the listener for the view
        setSearchSpinnerAdapter()

        ratingRdb.setOnClickListener(sortTypeListener)
        distanceRdb.setOnClickListener(sortTypeListener)

        startSearchIb.setOnClickListener( { tryStartSearch() })
        searchEt.setOnEditorActionListener(TextView.OnEditorActionListener { view, actionId, event ->
            Log.d(mTag, "setOnEditorActionListener($actionId, $event)")
            //Check for done action on the keyboard to start the search
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                tryStartSearch()
                return@OnEditorActionListener true
            }
            false;
        })
    }

    /**
     * sortTypeListener
     *
     * Listener for the Radio Button. change to distance or rating depending on user choice
     */
    private val sortTypeListener = View.OnClickListener { view ->
        when(view.id){
            R.id.rdb_distance -> searchPresenter.sortType = SortTypeParameter.DISTANCE
            R.id.rdb_rating -> searchPresenter.sortType = SortTypeParameter.RATING
        }
    }

    /**
     * tryStartSearch
     *
     * Try to request a new search to the [ISearchPresenter]
     */

    private fun tryStartSearch(){
        //Check if the search if not empty, do not launch the request if so
        val searchItem = searchEt.text
        if(searchItem.isEmpty()){
            searchEt.setBackgroundResource(R.drawable.shape_border_error)
            return
        }

        //Hide keyboard
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(searchEt.windowToken, 0)

        //Start the search
        searchPresenter.startSearch(currentCoordinate, searchItem.toString())
        searchEt.setBackgroundResource(R.drawable.shape_border)
    }

    //View interface fo presenter section....
    /**
     * onSearchSuccess()
     * Implementation of the [IViewSearch] to get the
     * interaction of the [ISearchPresenter]
     *
     * Create a new [BusinessPagerAdapter] to show of the result of the buisness
     * search of the query lauched before
     */

    override fun onSearchSuccess(businessList: List<Business>) {
        val mediaAdapter = BusinessPagerAdapter(supportFragmentManager, businessList)
        businessVp.adapter = mediaAdapter
    }

    /**
     * onSearchFailure()
     * Implementation of the [IViewSearch] to get the
     * interaction of the [ISearchPresenter]
     *
     * Print error dialog
     */

    override fun onSearchFailure(errorId: Int) {
        val error = getString(errorId)
        Log.e(mTag, "onSearchFailure($error)")
        val fragment = ErrorDialog.newInstance(errorId)
        fragment.show(this.fragmentManager, "Error dialog")
    }
    //END Presenter section

    //Spinner section
    /**
     * setSearchSpinnerAdapter()
     *
     * Create and set the Adapter from string ressource array to the searchType Spinner
     */

    private fun setSearchSpinnerAdapter(){
        val adapter = ArrayAdapter.createFromResource(this,
                R.array.search_type_array, android.R.layout.simple_spinner_item)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        searchTypeSpr.adapter = adapter
        searchTypeSpr.onItemSelectedListener = searchTypeListener;
    }

    /**
     * searchTypeListener
     *
     * Change the search parameter of enum [SearchParameter]
     * type: NAME, CUISINE TYPE or LOCATION
     */

    private val searchTypeListener = object: AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(p0: AdapterView<*>?) {/* UNUSED */ }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            //Use the position of the spinner box to get the right SearchParameter
            //by converting enum into and array
            val newSearchParam = SearchParameter.values()[position]
            Log.d(mTag, "onItemSelected($newSearchParam)")
            searchPresenter.searchParam = newSearchParam
        }
    }
    //END Spinner
}

package com.napworkshop.bellicious.networks

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.support.v4.content.ContextCompat
import android.util.Log
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.android.volley.Response
import com.android.volley.toolbox.ImageRequest
import com.napworkshop.bellicious.interfaces.IRequestBuilder

/**
 * VolleyNetwork
 * Implement the calls for the Yelp Api and contain the [Volley] queue
 * This class is a singleton so it can be called from anywhere an use the same queue
 * as suggested by the Google Documentation
 *
 * @param context, the application context
 *
 * @author m3can0
 * @created 2018-07-24
 */
class VolleyNetwork(context: Context){
    private val mTag = VolleyNetwork::class.java.simpleName

    //Static section...
    companion object {
        /**
         * getInstance()
         * Return the singleton instance of the class
         * @param context, the application context to use it anywhere
         *
         * @return the single instance of the class
         */
        @Volatile
        private var INSTANCE: VolleyNetwork? = null
        fun getInstance(context: Context) =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: VolleyNetwork(context)
                }
    }

    private val requestQueue: RequestQueue by lazy { Volley.newRequestQueue(context.applicationContext) }
    private val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    init{
        //Checking for permissions
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            throw Exception("Missing internet permission!!!")
        }
    }

    /**
     * addRequest()
     * Add any type of request extending the [IRequestBuilder] builder class
     *
     * @param   request the the request to add to the queue
     * @param   successlistener the callback to handle the result if successful
     * @param   errorListener   the callback to handle the result if failed
     * @return  false if there is no internet or error
     */

    fun <T>addRequest(request: IRequestBuilder<T>, successlistener: Response.Listener<T>, errorListener: Response.ErrorListener): Boolean{
        //Check if internet available
        if(!isInternetConnected()) {
            return false
        }

        //Add to queue
        Log.i(mTag, "Queue addition: ${request.build()}")
        requestQueue.add(YelpRequest<T>(request, successlistener, errorListener))
        return true
    }

    /**
     * getBitmap()
     * Retreive and image from an url...
     *
     * @param imageUrl the url where to get the image
     * @param   successlistener the callback to handle the result if successful
     * @param   errorListener   the callback to handle the result if failed
     * @return  false if there is no internet or error or null url
     */
    @Suppress("DEPRECATION")
    fun getBitmap(imageUrl: String?, successlistener: Response.Listener<Bitmap>, errorListener: Response.ErrorListener): Boolean{
        if(!isInternetConnected() || imageUrl == null) {
            return false
        }
        val imageRequest = ImageRequest(imageUrl, successlistener,
                0, 0, null, errorListener)
        requestQueue.add(imageRequest)
        return true
    }

    /**
     * isInternetConnected()
     *
     * Check if internet available
     *
     * @return true if internet available
     */

    protected fun isInternetConnected(): Boolean{
        return connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo.isConnected
    }
}
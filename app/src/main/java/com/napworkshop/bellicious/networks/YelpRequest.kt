package com.napworkshop.bellicious.networks

import com.android.volley.NetworkResponse
import com.android.volley.ParseError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.HttpHeaderParser
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.napworkshop.bellicious.interfaces.IRequestBuilder
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset

/**
 * YelpRequest
 * Class to create the volley request to get the informations from the server
 * @param request object [IRequestBuilder] containing all the information needed to create the request
 * @param successlistener listener to get the response of type T
 * @param errorListener listener for the error on the request
 *
 * @author m3can0
 * @created 2018-07-24
 */
open class YelpRequest<T>(private val request: IRequestBuilder<T>, private val successlistener: Response.Listener<T>, errorListener: Response.ErrorListener) : Request<T>(Method.GET, request.build(), errorListener) {
    private val gson = Gson()

    companion object {
        //TODO These values should be in a local gradle file not save by the get ignore to protect those infos
        val YELP_URL = "https://api.yelp.com/v3"
        private val APP_KEY = "c9-cV23P28WTK9lzS-bp3PJlBzjYaoeZjfzj8-nwCNKW_7SK5T1T02JkIGqUQW6Q9FPYJXfjiVRQfcBwyD0aEidhftjcfPtrep2Q57GYkb1K9qHp27omuXhNq6RXW3Yx"
    }

    /**
     * Modification to the header to be compliant with the Network Api
     */
    override fun getHeaders(): Map<String, String> {
        val headers = HashMap<String, String>()
        headers.put("Content-Type", "application/json")
        headers.put("Authorization", "Bearer %s".format(APP_KEY))
        return headers
    }

    /**
     * Gson parser here converting to string then use [Gson] to parse the query
     */
    override fun deliverResponse(response: T) = successlistener.onResponse(response)

    override fun parseNetworkResponse(response: NetworkResponse?): Response<T> {
        return try {
            val json = String(response?.data ?: ByteArray(0), Charset.forName(HttpHeaderParser.parseCharset(response?.headers)))
            val search = gson.fromJson(json, request.clazz)
            Response.success(search, HttpHeaderParser.parseCacheHeaders(response))
        } catch (e: UnsupportedEncodingException) {
            Response.error(ParseError(e))
        } catch (e: JsonSyntaxException) {
            Response.error(ParseError(e))
        }
    }
}
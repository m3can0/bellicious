package com.napworkshop.bellicious.networks

import com.napworkshop.bellicious.enums.SearchParameter
import com.napworkshop.bellicious.enums.SortTypeParameter
import com.napworkshop.bellicious.interfaces.IRequestBuilder
import com.napworkshop.bellicious.models.Coordinate
import com.napworkshop.bellicious.models.SearchResponse

/**
 * SearchRequestBuilder
 * Class that contain and handle all the variables for the /businesses/search route of Yelp
 *
 * It built the url
 *
 * @author m3can0
 * @created 2018-07-26
 */
class SearchRequestBuilder(val coordinate: Coordinate, val searchParam: SearchParameter, var sort: SortTypeParameter) : IRequestBuilder<SearchResponse>(){

    override val clazz: Class<SearchResponse> = SearchResponse::class.java
    override val route: String = "businesses/search"

    override fun build(): String{
        val builder = StringBuilder()

        //required field by Api
        builder.append("${YelpRequest.YELP_URL}/$route")
                //Add coordinate
                .append(coordinate.formatToUrlParam())
                //Sort type
                .append(sort.formatToUrlParam())
                //Limit number for search to reduce parsing time
                .append("&limit=20")
                //Addintionnal params
                .append(searchParam.formatToUrlParam())


        return builder.toString()
    }
}
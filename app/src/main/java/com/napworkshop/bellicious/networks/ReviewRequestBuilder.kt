package com.napworkshop.bellicious.networks

import com.napworkshop.bellicious.interfaces.IRequestBuilder
import com.napworkshop.bellicious.models.Business
import com.napworkshop.bellicious.models.Review
import com.napworkshop.bellicious.models.ReviewResponse

/**
 * ReviewRequestBuilder
 * Class that contain and handle all the variables for the /businesses/{id} route of Yelp
 *
 * It built the url
 *
 * @param id the string id of the buisiness needed
 * @author m3can0
 * @created 2018-07-26
 */
class ReviewRequestBuilder(val id: String, val language: Language = Language.FR) : IRequestBuilder<ReviewResponse>() {

    override val clazz: Class<ReviewResponse> = ReviewResponse::class.java
    override val route: String = "businesses"

    override fun build(): String {
        return "${YelpRequest.YELP_URL}/$route/$id/reviews"
    }

    //?locale=${language.param}
    enum class Language(val param: String){
        FR("fr_CA"), EN("en_CA");
    }
}
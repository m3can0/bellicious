package com.napworkshop.bellicious.networks

import com.napworkshop.bellicious.interfaces.IRequestBuilder
import com.napworkshop.bellicious.models.Business

/**
 * BusinessRequestBuilder
 * Class that contain and handle all the variables for the /businesses/{id} route of Yelp
 *
 * It built the url
 *
 * @param id the string id of the buisiness needed
 * @author m3can0
 * @created 2018-07-26
 */
class BusinessRequestBuilder(val id: String) : IRequestBuilder<Business>() {

    override val clazz: Class<Business> = Business::class.java
    override val route: String = "businesses"

    override fun build(): String {
        return "${YelpRequest.YELP_URL}/$route/$id"
    }
}
package com.napworkshop.bellicious

import android.app.DialogFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.TextView

import java.util.logging.Logger

/**
 * ErrorDialog
 * Error dialog to show error
 *
 * @author m3can0
 * @since 2018-03-17
 */

class ErrorDialog : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootview = inflater.inflate(R.layout.dialog_error, container, false)

        //Ajoute le boutoon pour ne plus voir la vue
        val dismissBtn = rootview.findViewById<Button>(R.id.btn_dismiss)
        dismissBtn.setOnClickListener({ this@ErrorDialog.dismiss() })

        //Get the error id from the bundle
        val errorId = arguments.getInt(ARGS_ERROR_ID)

        //Set the error in body
        val errorTv = rootview.findViewById<TextView>(R.id.tv_error_body)
        errorTv.setText(errorId)

        //Remove title
        dialog.window.requestFeature(Window.FEATURE_NO_TITLE);

        return rootview
    }

    companion object {
        private val LOG = Logger.getLogger(ErrorDialog::class.java.simpleName)

        private val ARGS_ERROR_ID = "error_id"

        /**
         * newInstance create a new instance with the error id
         */

        fun newInstance(errorId: Int): ErrorDialog {
            val dialog = ErrorDialog()

            val args = Bundle()
            args.putInt(ARGS_ERROR_ID, errorId)
            dialog.setArguments(args)

            return dialog
        }
    }
}

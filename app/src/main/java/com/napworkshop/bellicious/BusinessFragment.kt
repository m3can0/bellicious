package com.napworkshop.bellicious

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.napworkshop.bellicious.models.Business
import com.napworkshop.bellicious.networks.VolleyNetwork
import android.graphics.Bitmap
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import com.android.volley.Response
import com.napworkshop.bellicious.models.Categorie
import com.napworkshop.bellicious.models.Hour
import com.napworkshop.bellicious.networks.BusinessRequestBuilder
import android.widget.RelativeLayout



/**
 * BusinessFragment
 * Fragment pour contenir chaque objet Business
 *
 * @author m3can0
 * @created 2018-07-25
 */
class BusinessFragment: Fragment() {
    private val mTag = BusinessFragment::class.java.simpleName

    /**
     * Extra singleton for the extra of the [Bundle]
     */

    object Extra{
        val BUSINESS= "com.napworkshop.poc.videotransition.extra.business"
    }

    /**
     * Builder singleton to create a new fragment
     *
     * Handle adding arguments, flags, etc... for you
     */
    object Builder{
        fun newInstance(business: Business): BusinessFragment{
            val fragment = BusinessFragment()
            val args= Bundle()
            args.putParcelable(Extra.BUSINESS, business)
            fragment.arguments= args
            return fragment
        }
    }

    private lateinit var rootView: View
    private val mImageView:  ImageView by lazy { rootView.findViewById<ImageView>(R.id.iv_business_image) }
    private val mNameTv:    TextView by lazy { rootView.findViewById<TextView>(R.id.tv_business_name) }
    private val mAddressnTv:    TextView by lazy { rootView.findViewById<TextView>(R.id.tv_business_address) }
    private val mPhoneTv:    TextView by lazy { rootView.findViewById<TextView>(R.id.tv_business_phone) }
    private val mRatingRb: RatingBar by lazy { rootView.findViewById<RatingBar>(R.id.rb_business_rating) }
    private val mDayMondayTv:    View by lazy { rootView.findViewById<View>(R.id.v_day_monday) }
    private val mDayTuesdayTv:    View by lazy { rootView.findViewById<View>(R.id.v_day_tuesday) }
    private val mDayeWednesdayTv:    View by lazy { rootView.findViewById<View>(R.id.v_day_wednesday) }
    private val mDayThursdayTv:    View by lazy { rootView.findViewById<View>(R.id.v_day_thursday) }
    private val mDayFridayTv:    View by lazy { rootView.findViewById<View>(R.id.v_day_friday) }
    private val mDaySaturdayTv:    View by lazy { rootView.findViewById<View>(R.id.v_day_saturday) }
    private val mDaySundayTv:    View by lazy { rootView.findViewById<View>(R.id.v_day_sunday) }
    private val mDaysTv:    LinearLayout by lazy { rootView.findViewById<LinearLayout>(R.id.ll_days) }
    private val mCategoriesTv:    LinearLayout by lazy { rootView.findViewById<LinearLayout>(R.id.ll_tags) }

    private lateinit var business: Business
    private lateinit var volley: VolleyNetwork

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView= inflater.inflate(R.layout.fragment_business, container, false)

        volley = VolleyNetwork.getInstance(this.context!!)

        //Retreive the arguments from the bundle
        business = arguments?.get(Extra.BUSINESS) as Business

        //Update view with business values
        mNameTv.text = business.name

        //Format the address on 3 lines
        val addressArray = arrayOfNulls<String>(business.location.displayAddress.size)
        (business.location.displayAddress as ArrayList).toArray(addressArray)

        val addressBuilder = StringBuilder()
        for(line: String? in business.location.displayAddress){
            if(line != null){
                addressBuilder.append(line).append("\n")
            }
        }
        mAddressnTv.text = addressBuilder

        mPhoneTv.text = business.phone
        mRatingRb.rating = business.rating

        //Get more details
        getBusinessImage(business)
        getBusinessDetails(business)

        return rootView
    }

    private fun getBusinessImage(business: Business){
        volley.getBitmap(business.imageUrl!!, object: Response.Listener<Bitmap>{
            override fun onResponse(bitmap: Bitmap) {
                mImageView.setImageBitmap(bitmap)
            }
        }, Response.ErrorListener {
            Log.e(mTag, "failed getting ${business.imageUrl}")
            mImageView.setImageResource(R.drawable.ic_toque)
        })
    }

    private fun getBusinessDetails(business: Business){
        //Get more precise info on business
        volley.addRequest(BusinessRequestBuilder(business.id), object: Response.Listener<Business>{
            override fun onResponse(business: Business) {
                Log.i(mTag, business.toString() )
                this@BusinessFragment.business = business

                //Setting the regular hour to the view
                setBusinessHours(business.hourList)

                //Setting tags
                setCategories(business.categoryList)
            }
        }, Response.ErrorListener { Log.e(mTag, "failed getting business with id ${business.id}") })
    }

    /**
     * setBusinessHours()
     *
     * set the business hours if the Api return something
     */

    private fun setBusinessHours(hourList: List<Hour>?) {
        //Hide container if no hours
        if(hourList == null || hourList.isEmpty()){
            mDaysTv.visibility = View.GONE
        }
        else{
            mDaysTv.visibility = View.VISIBLE
        }

        //for every day add the hour
        hourList?.filter { it.type == "REGULAR" }
                ?.forEach {
                    it.businessHourList.forEach{
                        when(it.day){
                            0 -> setDay(mDayMondayTv, it, R.string.day_monday)
                            1 -> setDay(mDayTuesdayTv, it, R.string.day_tuesday)
                            2 -> setDay(mDayeWednesdayTv, it, R.string.day_wednesday)
                            3 -> setDay(mDayThursdayTv, it, R.string.day_thursday)
                            4 -> setDay(mDayFridayTv, it, R.string.day_friday)
                            5 -> setDay(mDaySaturdayTv, it, R.string.day_saturday)
                            6 -> setDay(mDaySundayTv, it, R.string.day_sunday)
                        }
                    }
                }

    }

    private fun setDay(rootView: View, businessHour: Hour.BusinessHour, dayStringId: Int){
        //Convert the 4 digit to HH:mm
        val startHour = businessHour.start.subSequence(0, 2)
        val startMin = businessHour.start.subSequence(1, 3)
        val endHour = businessHour.end.subSequence(0, 2)
        val endMin = businessHour.end.subSequence(1, 3)
        val builder = StringBuilder()
        builder.append(startHour).append(":").append(startMin)
                .append("\n--\n")
        builder.append(endHour).append(":").append(endMin)

        //Change to the view
        rootView.visibility = View.VISIBLE
        rootView.findViewById<TextView>(R.id.tv_day_title).setText(dayStringId)
        rootView.findViewById<TextView>(R.id.tv_day_hours).text = builder
    }

    /**
     * setCategories()
     *
     * add all the categories to the view
     */

    private fun setCategories(categoryList: List<Categorie>?) {
        categoryList?.forEach {
            val itemTv = layoutInflater.inflate(R.layout.item_category, null) as TextView
            mCategoriesTv.addView(itemTv)
            itemTv.text =it.title
        }
    }
}
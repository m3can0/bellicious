package com.napworkshop.bellicious.presenters

import android.util.Log
import com.android.volley.Response
import com.napworkshop.bellicious.R
import com.napworkshop.bellicious.enums.SearchParameter
import com.napworkshop.bellicious.enums.SortTypeParameter
import com.napworkshop.bellicious.models.Business
import com.napworkshop.bellicious.models.Coordinate
import com.napworkshop.bellicious.models.SearchResponse
import com.napworkshop.bellicious.networks.SearchRequestBuilder
import com.napworkshop.bellicious.networks.VolleyNetwork

/**
 * ISearchPresenter
 * Presenter to abstract all the controller logic by using the [ISearchView]
 * interface to abstract the external control of the view.
 *
 * @param network the VolleyNetwork to do the network command
 * @param view the interface to interact with the presenter
 *
 * @author m3can0
 * @created 2018-07-24
 */
class ISearchPresenter(private val network: VolleyNetwork, val view: ISearchView){
    private val mTag = ISearchPresenter::class.java.simpleName

    //Variables to change params of the queries
    var sortType = SortTypeParameter.DISTANCE
    var searchParam = SearchParameter.NAME

    /**
     * startSearch()
     *
     * Start a search request to the [VolleyNetwork].
     * It use the variables over as the parameters for the query
     *
     * @userSearch word or series of words inputed by the user to search for a restaurant
     */

    fun startSearch(coordinate: Coordinate, userSearch: String){
        //Remove space and capital espacially important pour category
        val formattedInput = userSearch.replace(" ", "").toLowerCase()

        //If the cuisine add the cuisine type before the query
        if(searchParam == SearchParameter.CUISINE_TYPE){
            formattedInput.startsWith("cuisine,")
        }

        //Create and do the request
        searchParam.value = formattedInput
        val request = SearchRequestBuilder(coordinate, searchParam, sortType)

        //Send the request to volley or other network implementation
        if(!network.addRequest(request, searchRequestSuccessListener, searchRequestErrorListener)){
            //Send error if no Internet
            view.onSearchFailure(R.string.error_network_connection)
        }
    }

    /**
     * searchRequestSuccessListener
     *
     * Listener for the specific search call the listener to send to the view
     */

    private val searchRequestSuccessListener = Response.Listener<SearchResponse> { response ->
        Log.i(mTag, "Yelp SearchResponse: $response")
        //send back the response to the view
        view.onSearchSuccess(response.businessList)
    }

    /**
     * searchRequestErrorListener
     *
     * Listener for the specific search call send the error to the view
     */

    private val searchRequestErrorListener = Response.ErrorListener{ response ->
        Log.e(mTag, "Yelp Error(${response.networkResponse.statusCode}): ${response.networkResponse.data}")
        //Send error if server error to teh view
        view.onSearchFailure(R.string.error_network_server)
    }

    /**
     * ISearchView
     *
     * Implementation that the view has to implements to receive the action of the presenter
     */

    interface ISearchView{
        /**
         * callback in case of error
         *
         * @return errorId, the string ressource related to the error
         */
        fun onSearchFailure(errorId: Int)

        /**
         * callback in case case of success
         *
         * @return businessList, the list of buisiness found for the query
         */
        fun onSearchSuccess(businessList: List<Business>)
    }
}
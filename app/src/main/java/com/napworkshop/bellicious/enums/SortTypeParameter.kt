package com.napworkshop.bellicious.enums

import com.napworkshop.bellicious.interfaces.IParameter

/**
 * SortTypeParameter
 * Enum that list the sort type
 *
 * @author m3can0
 * @created 2018-07-24
 */
enum class SortTypeParameter( override var value: String ): IParameter {
    RATING("rating"),
    DISTANCE("distance");

    override val paramName = "sort_by"
}
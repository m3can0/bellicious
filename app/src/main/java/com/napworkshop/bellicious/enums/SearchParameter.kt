package com.napworkshop.bellicious.enums

import com.napworkshop.bellicious.interfaces.IParameter

/**
 * SortTypeParameter
 * Enum to get the search type
 *
 * @author m3can0
 * @created 2018-07-24
 */
enum class SearchParameter(override val paramName: String): IParameter {
    NAME("term"),
    LOCATION("coordinate"),
    CUISINE_TYPE("categories");

    override lateinit var value: String
}
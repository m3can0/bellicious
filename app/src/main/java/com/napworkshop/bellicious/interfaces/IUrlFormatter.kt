package com.napworkshop.bellicious.interfaces

/**
 * IUrlFormatter
 * Class to format a part or whole url.
 *
 * @author m3can0
 * @created 2018-07-24
 */
interface IUrlFormatter{
    /**
     * formatToUrlParam
     * format for url
     * @return the formatted param
     */
    fun formatToUrlParam(): String
}
package com.napworkshop.bellicious.interfaces

/**
 * QueryBuilder
 * Classe qui fait quelque chose
 *
 * @author m3can0
 * @created 2018-07-24
 */
abstract class IRequestBuilder<T>{

    abstract val route: String
    abstract val clazz: Class<T>

    abstract fun build(): String
}
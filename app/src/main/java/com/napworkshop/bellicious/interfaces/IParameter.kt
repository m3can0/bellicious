package com.napworkshop.bellicious.interfaces

/**
 * IParameter
 * class to get format a param and a value for an url
 *
 * @author m3can0
 * @created 2018-07-24
 */
interface IParameter: IUrlFormatter {
    val paramName: String
    var value: String

    /**
     * formatToUrlParam
     * add the & and the = to create a new parameter for url
     */
    override fun formatToUrlParam(): String{
        return String.format("&%s=%s", paramName, value)
    }
}